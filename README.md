
# Projet Blog
Dans le cadre de ce projet, j'ai réalisé un blog grâce à Symfony pour le back et React pour le front.
Les wiframes desktop et mobiles ont été réalisé à l'aide de [Figma](https://www.figma.com/file/9uW9Z9i44cNDhzpRIq4QFu/Blog?node-id=0%3A1&t=Wr5TamMmUX5Uy0Da-1). 

Le côté back a été réalisé avec MariaDB et Symfony, en créant une base de données et un script SQL, puis en créant des entités, des repository et des controllers. Pour l'entité "Article", un ArticleType a été créé (il reste à faire les "Types" pour les autres entités à l'avenir).
Un système de validation des entités a été mis en place grâce aux validateurs dans les entités et au système de try-catch utilisé lors de la deserialization pour renvoyer une erreur.

Le côté front a été réalisé à l'aide de React, Next et bootstrap.
On a crée un entité Article, une page d'accueil, une page d'ajout d'article et une page de détail d'un article. Les méthodes sont appelées dans la partie article-service, le css a été géré de façon global via les fichiers global.css et _app.tsx. Des components ont été créés pour la barre de navigation, le footer, un item Article et également pour les articles populaires qui reste à compléter avec une condition pour afficher dans l'ordre seulement les 5 articles les plus consultés.
 

# Class Diagram
![<alt>](</public/images/Diagram de classes.png>)

# Wireframes mobile
![<alt>](</public/images/Wireframes mobiles.png>)

# Wireframes desktop
![<alt>](</public/images/Wireframes desktop.png>)

# Rendu Front desktop
![<alt>](</public/images/FrontBlog.png>)

# Consignes

L'objectif du projet est de créer une petite application de blog sans authentification en utilisant Symfony pour le backend et React pour le frontend.

Fonctionnalités attendues:
Créer des articles
Consulter la liste des articles
Consulter un article spécifique
Modifier ou supprimer un article

La gestion des users n'est pas attendue, on va considérer que tout le monde peut poster un article en mettant juste son nom dans le formulaire.

Fonctionalités bonus:
Ajouter une barre de recherche pour les articles
Permettre des commentaires sur les posts
Ajouter des catégories pour les articles
Ajouter un compteur de vue sur les posts


Travail attendu:
Créer les wireframes des différentes pages de l'application (mobile first)
Créer le script de mise en place de la base de données et les composants d'accès aux données pour la ou les tables avec PDO (repository)
Créer une API Rest avec Symfony (les contrôleurs et la validation)
Créer le front (responsive) de l'application avec React/Next
Requêter l'API Rest depuis des services de typescript utilisés dans les components React


## Les liens concernant le projet
- [ ] [Gitlab symfony](https://gitlab.com/Laure_G/blog-symfony)
- [ ] [Gitlab react](https://gitlab.com/Laure_G/blog-react)

## Les outils utilisés
- StarUML pour les diagrammes de classes, 
- MariaDB pour la base de donnée MySQL, 
- Symfony pour le back,
- React pour le front,
- Gitlab.

## Test 
Les CRUD ont été testés à l'aide du "Thunder Client".

## Statut du projet
Certaines fonctionnalités ont été mises en option par manque de temps et restent donc à travailler par la suite.

