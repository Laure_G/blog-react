import { Article } from "@/entities";
import Link from "next/link";

interface Props {
    article: Article;
}

export default function ItemArticle({article}: Props) {
    
    return(
       
     <div className="row">
        <div className="col-6">
        <Link href={"/article/"+article.id}><img src={article.mainPicture} className="card-img-top" alt="..." style={{height: 6+"rem"}}/></Link>
        </div>
        <div className="col-6">
        <h5 className="populartitre">{article.title}</h5>
        </div>
        
    </div>
       
        );
    }