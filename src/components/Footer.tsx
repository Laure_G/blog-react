

export default function Footer() {
    
    return(
        <div className="row footer d-flex justify-content-between pt-4 pb-1">
            <div className="col-sm-6 col-md-5 text-left ps-4">
        <img src="/stairslogo.png" alt="Logo" width="50" height="44" className="d-inline-block align-text-center"/> Archi&Stairs
            </div>
            <div className="col-sm-6 col-md-5 text-end pe-4">
            <img src="/logoRS.png" alt="Logo réseaux sociaux"  height="44" className="d-inline-block align-text-center"/> 
            </div>
            <div className="col-sm-10 col-md-12 text-center">
            <button type="button" className="btn btn-dark"> Contactez-nous</button>
            <p className="mt-4 ">Confidentialité / Politique relative aux cookies / Mentions légales / Accessibilité</p>
            </div>
        </div>
    )

}