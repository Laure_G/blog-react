import { Article } from "@/entities";
import Link from "next/link";

interface Props {
    article: Article;
}

export default function ItemArticle({article}: Props) {
    
    return(
       
        
     <div className="mt-4">
        <img src={article.mainPicture} className="card-img-top" alt={article.title} style={{height: 20+"rem"}}/>
        <div className="card-body">
        <h5 className="card-title mt-3">{article.title}</h5>
        <p className="card-text mt-3">{article.content?.split(' ').slice(0, 14).join(' ')}..</p>
        <Link href={"/article/"+article.id} className="btn btn-dark btn-sm">Lire l'article</Link>
        </div>
    </div>
        
           
        );
    }