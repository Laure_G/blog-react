import Link from "next/link";

export default function Navbar() {
    
    return(
    <nav className="navbar navbar-expand-lg ps-3">
        <div className="container-fluid">
        <Link className="navbar-brand" href={"http://localhost:3000/"}>
        <img src="/stairslogo.png" alt="Logo" width="50" height="44" className="d-inline-block align-text-center"/> Archi&Stairs
        </Link>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
        <Link className="nav-link active" aria-current="page" href={"http://localhost:3000/"}>Accueil</Link>
        </li>
        <li className="nav-item">
        <Link className="nav-link " href={"/add/"}>Ajouter un article</Link>
        </li>
        </ul>
        </div>
        </div>
        
        </nav>

        
        );
    }