export interface Article {
    title?:string;
    mainPicture?:string;
    content?: string;
    author?: string;
    date?: string;
    id? : number;
}