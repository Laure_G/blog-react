import { postArticle } from "@/article-service";
import Footer from "@/components/Footer";
import FormArticle from "@/components/FormArticle";
import Navbar from "@/components/Navbar";
import { Article } from "@/entities";
import { useRouter } from "next/router";

export default function Add() {
    const router = useRouter();

    async function addArticle(article: Article) {
        const added = await postArticle(article);
        router.push('/article/' + added.id);
    }

    return(
        <div>
        <div className="formulaireAjout">
             <Navbar/>
            <h1 className="formulaireTitre text-center m-5">Ajouter un nouvel article</h1>
            <FormArticle onSubmit={addArticle} />
        </div>
         <Footer/>
         </div>
    )
}