import { deleteArticle, fetchOneArticle, updateArticle } from "@/article-service";
import Footer from "@/components/Footer";
import FormArticle from "@/components/FormArticle";
import Navbar from "@/components/Navbar";
import { Article } from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function ArticlePage() {
    
    const router = useRouter();
    const {id} = router.query
    
    const [article,setArticle]= useState<Article>();
    const [showEdit, setShowEdit] = useState(false);
    useEffect(()=>{
        if(!id){
            return;
        }
        fetchOneArticle(Number(id))
        .then(data=> setArticle(data))
        .catch(error => {
            console.log (error);
            if(error.reponse.status == 404){
                router.push('/404');
            }
        });
    }, [id]);
    
    
    async function remove() {
        await deleteArticle(id);
        router.push('/');
    }
    async function update(article:Article) {
        const updated = await updateArticle(article);
        setArticle(updated);
    }
    
    function toggle() {
        setShowEdit(!showEdit);
    }
    
    if (!article) {
        return <p>Loading...</p>
    }
    
    
    return (
        <div>
        <div className="text-center">
        <Navbar/>
        <h1 className="mt-4">{article?.title}</h1>
        <div className="row d-flex justify-content-center">
        <div className="col-sm-10 col-md-10">
        <p>{article?.author} - {article?.date && new Date(article?.date).toLocaleDateString()}</p>
        <img className="mt-4 mb-4" src={article?.mainPicture} alt={article?.title}/>
        <p className="mt-4 mb-4"> {article?.content} </p>

{showEdit &&
            <>
            <h2 className="mt-4"  >Modifier l'article</h2>
         
            <FormArticle edited={article} onSubmit={update} />
      
            </>
        }

       
        <button  className="btn btn-dark btn-sm pe-3 ps-3 mt-4 me-2 mb-4" onClick={toggle}>Modifier</button>
        <button className="btn btn-dark btn-sm pe-3 ps-3 mt-4 ms-2 mb-4" onClick={remove}>Supprimer</button>
        
        
        </div>
        </div>
        
        
       
        </div>
         <Footer/>
         </div>
        )
    }
    
    
    