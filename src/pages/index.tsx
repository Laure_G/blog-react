import { fetchAllArticles } from "@/article-service";
import Footer from "@/components/Footer";
import ItemArticle from "@/components/ItemArticle";
import Navbar from "@/components/Navbar";
import PopularArticle from "@/components/PopularArticle";
import { Article } from "@/entities";
import { useEffect, useState } from "react";

export default function Index() {

    const [articles,setArticles]= useState<Article[]>([])
    useEffect(()=>{
       fetchAllArticles().then(data=>{
        setArticles(data);
       });
    }, [])
    

    return(
        <div>
            <div className="col header mb-5">
                <Navbar/>
                
                <h1 className="text-center mb-2 pb-0">Architecture & Design</h1>
               <h2 className="text-center mt-1 pt-0">Les plus beaux escaliers à travers le monde</h2>
            </div>
            
            <div className="row d-flex justify-content-around ">
                <div className="col-sm-10 col-md-9">
                    <div className="row d-flex justify-content-around">
                        {articles.map((item) =>
                         <div className="col-sm-10 col-md-5  text-center mt-4 mb-4" >
                        <ItemArticle key= {item.id} article ={item}/> </div>
                            )}
                    </div>
                </div>
                <div className="col-sm-10 col-md-3 mt-4 ">
                    <h2>Articles populaires</h2>
                    {articles.map((item) =>
                         <div className="col mt-3" >
                        <PopularArticle key={item.id} article={item}/> </div>
                            )}
                </div>
            </div>
           
           <Footer/>
            
        </div>
    )

}